const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');

// Import Authentication Routes
const authRoute = require('./routes/auth');
// Import View Page Routes
const pageRoute = require('./routes/page');

dotenv.config();

// Connect to DB
mongoose.connect(process.env.DB_CONNECT, { useUnifiedTopology: true }, () => {
    console.log('Connected to DB')
});

//Middleware
app.use(express.json());

// Route Middleware
app.use('api/user', authRoute);
app.use('api', pageRoute);

app.listen(8000, () => console.log("Server up and running"));