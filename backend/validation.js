const Joi = require('@hapi/joi');

//Registration Validation
const registerValidation = (data) => {
    const schema = {
        firstName: Joi.string().min(2).required(),
        lastName: Joi.string().min(2).required(),
        email: Joi.string().min(3).required().email(),
        password: Joi.string().min(8).required()
    }
    return Joi.validate(data, schema)
}

//Login Validation
const loginValidation = (data) => {
    const schema = {
        email: Joi.string().min(3).required().email(),
        password: Joi.string().min(8).required()
    }
    return Joi.validate(data, schema)
}

//Create Organization Validation
const createOrganizationValidation = (data) => {
    const schema = {
        organizationName: Joi.string().required(),
        owner: Joi.string().required(),
        address: Joi.string().required(),
        city: Joi.string().required(),
        state: Joi.string(),
        country: oi.string().required()
    }
    return Joi.validate(data, schema)
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.createOrganizationValidation = createOrganizationValidation;