const router = require('express').Router();
const Organization = require('../model/Organization');

router.post('/add', async (req, res) => {
    const organization = new Organization({
        organizationName: req.body.organizationName,
        owner: req.body.owner,
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        country: req.body.country
    });
    try{
        const saveOrganization = await organization.save();
        res.send({organization: organization._id});
    }catch(err){
        res.status(400).send(err);
    }
});

router.post('/edit', async (req, res) => {
    const queryOrganization = { 
        organizationName: req.body.organizationName
    };
    const updateOrganization = {
        owner: req.body.owner,
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        country: req.body.country
    }
    try{
        const updateDB = await organization.update();
    }catch(err){
        res.status(400).send(err);
    }
});

router.get('/', async (req, res) => {
    // Read and retrieve all organization data
    return await Organization.find();
});